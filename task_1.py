"""
Написати функцію, яка приймає 2 аргументи – цілі числа. Всередині функції виконується
перевірка типу. Якщо хоча б одне з них не int, то повертається 1, якщо обидва int, то
рахується їхня сума. Якщо сума додатня, повертається 0, якщо від’ємна, то -1.
"""
from numbers import Number


def number_preparing(arg_1: Number, arg_2: Number, /) -> Number:
    if all((isinstance(arg_1, int), isinstance(arg_2, int))):
        return 0 if (arg_1 + arg_2) >0 else -1
    return 1


if __name__ == "__main__":
    cases = [
        (1, 2.3, 1),
        ("str", 23, 1),
        (1, 1, 0),
        (-23, 4, -1),
    ]

    for arg_1, arg_2, res in cases:
        assert number_preparing(arg_1, arg_2) == res, f"ERROR! number_preparing{arg_1, arg_2} = {number_preparing(arg_1, arg_2)} but expected {res}"